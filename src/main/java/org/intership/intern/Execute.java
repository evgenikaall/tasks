package org.intership.intern;

import java.time.LocalDate;
import java.util.Map;
import java.util.TreeMap;

public class Execute {
    public static void main(String[] args) {
        StudentMap<Student, Integer> map = new StudentMap<>(new StudentComparator());
        TreeMap<Student, Integer> q = new TreeMap<>(new StudentComparator());

//        Student f = new Student("f",LocalDate.of(2020,3,2), "123");
//        Student b = new Student("b",LocalDate.of(2020,3,2), "2");
//        Student a = new Student("a",LocalDate.of(2020,3,2), "3");
//        Student d = new Student("d",LocalDate.of(2020,3,2), "4");
//        Student g = new Student("g",LocalDate.of(2020,3,2), "5");
//        Student i = new Student("i",LocalDate.of(2020,3,2), "55");
//
//        map.put(f, 1);
//        map.put(b, 2);
//        map.put(a, 3);
//        map.put(d, 4);
//        q.put(g, 5);
//        q.put(i, 55);
//        map.putAll(q);
//
//        System.out.println(map);

        Map<String, Integer> test1 = new StudentMap<>();

        map.put(f, 1);
        map.put(b, 2);
        map.put(a, 3);
        map.put(d, 4);
        q.put(g, 5);
        q.put(i, 55);
        map.putAll(q);

    }
}
